import { Resource } from "./resource";
import { UmlClass } from "./uml-class";

export class GeneralizationSet extends Resource {
    parent: UmlClass;
    children: Array<UmlClass> = [];

    constructor(id:string,parent: UmlClass,name?:string) {
        super(id,name)
        this.parent = parent;
    }
}