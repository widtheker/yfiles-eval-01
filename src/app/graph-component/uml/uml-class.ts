import { Actor } from "./actor";
import { Attribute } from "./attribute";

export class UmlClass extends Actor {
    constructor(id: string,name: string,attributes?: Array<Attribute>) {
        super(id, name,attributes);
    }
}