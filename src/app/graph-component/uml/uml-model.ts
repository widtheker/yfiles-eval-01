import { Actor } from "./actor";
import { Aggregation } from "./aggregation";
import { Attribute } from "./attribute";
import { GeneralizationSet } from "./generalisation-set";
import { Role } from "./role";
import { UmlClass } from "./uml-class";
import { UmlEnum } from "./uml-enum";

export class UmlModel {
    actors: Array<Actor> = [];
    aggregations: Array<Aggregation> = [];
    generalizationSets: Array<GeneralizationSet> = [];

    constructor() {
        let ip = new UmlClass("1","Vehicle")
        this.actors.push(ip)
        let country = new UmlClass("2", "Country")
        this.actors.push(country)
        let customer = new UmlClass("3","Ambulance")
        this.actors.push(customer)
        let unit = new UmlClass("4","Car")
        this.actors.push(unit)
        let relationship = new UmlClass("5","Authorized Driver")
        this.actors.push(relationship)
        let lcstatus = new UmlEnum("6","Driver Life-Cycle Status")
        this.actors.push(lcstatus)
        this.aggregations.push(new Aggregation(new Role("7",ip,undefined, "0..*"),new Role("8",country,"Current Location","1")))
        this.aggregations.push(new Aggregation(new Role("9",customer,undefined, "0..*"),new Role("10",unit,"Preferred Vehicle Type","1")))
        this.aggregations.push(new Aggregation(new Role("11",customer,undefined, "1"),new Role("12",relationship,undefined,"0..*")))
        this.aggregations.push(new Aggregation(new Role("13",relationship,undefined, "0..*"),new Role("14",lcstatus,undefined,"1")))
        let roleType = new GeneralizationSet("15",ip,"Role Type")
        roleType.children.push(customer)
        this.generalizationSets.push(roleType)
        let ipType = new GeneralizationSet("16",ip,"Type")
        ipType.children.push(unit)
        this.generalizationSets.push(ipType)
        ip.attributes.push(new Attribute("17","Chassis Number"))
        country.attributes.push(new Attribute("18","Country Code"))
        lcstatus.attributes.push(
            new Attribute("19","Active Driver"),
            new Attribute("20","Dormant Driver"),
            new Attribute("21","Former Driver"),
            new Attribute("22","Pending Driver"),
            new Attribute("23","Potential Driver")
        )
    }
}