import { Actor } from "./actor";
import { Resource } from "./resource";

export class Role extends Resource {
    cardinality: string | undefined;
    actor: Actor;

    constructor(id: string, actor: Actor, name?: string, cardinality?: string) {
        super(id,name)
        this.cardinality = cardinality;
        this.actor = actor;
    }
}