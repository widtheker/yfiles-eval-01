import { RotatableNodePlacerBase } from "yfiles/*";
import { Role } from "./role";

export class Aggregation {
    whole: Role;
    part: Role;

    constructor(whole: Role, part: Role) {
        this.whole = whole;
        this.part = part;
    }
}