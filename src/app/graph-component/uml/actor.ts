import { Attribute } from "./attribute";
import { Resource } from "./resource";

export class Actor extends Resource {
    attributes: Array<Attribute>;
    constructor(id: string, name:string, attributes?:Array<Attribute>) {
        super(id,name);
        this.name = name;
        this.attributes = attributes || [];
    }

}