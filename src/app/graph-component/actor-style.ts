import { Fill, INode, IRenderContext, NodeStyleBase, Size, Stroke, SvgVisual, Visual } from "yfiles/*";
import { Actor } from "./uml/actor";

const SVGNS = 'http://www.w3.org/2000/svg'

export class ActorNodeStyle extends NodeStyleBase {

    private actor: Actor;

    private readonly borderFill = Fill.from('#2C4B52')
    private readonly headerHeight = 20
    private readonly rowHeight = 20

    constructor(actor?: Actor) {
        super()
        this.actor = actor || new Actor("99","Dummy")
      }
    
    protected createVisual(context: IRenderContext, node: INode): Visual | null {
        const nodeSize = node.layout.toSize()
        const g = document.createElementNS(SVGNS, 'g')
        this.render(g, nodeSize, context)
        SvgVisual.setTranslate(g, node.layout.x, node.layout.y)
        return new SvgVisual(g)
    }

    render(g: SVGGElement, nodeSize: Size, context: IRenderContext) {
        // draw the rect with a thick border
        const borderThickness = 4
        const borderStroke = new Stroke({
            fill: this.borderFill,
            thickness: borderThickness
        })

        // dont understand
        if (nodeSize.height < this.headerHeight) {
            return g
        }

        // render a header which is used as background for the label
        const header = document.createElementNS(SVGNS, 'rect')
        header.width.baseVal.value = nodeSize.width
        header.height.baseVal.value = this.headerHeight
        this.borderFill.applyTo(header, context)
        g.appendChild(header)

    
        return g
    }


}