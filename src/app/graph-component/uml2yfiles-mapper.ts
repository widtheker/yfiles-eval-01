import { GraphTransformer, IGraph, INode, InteriorStretchLabelModel, NinePositionsEdgeLabelModel, Rect } from 'yfiles';
import { UmlModel } from "./uml/uml-model";

export class Uml2YFilesMapper {

    static getGraph(umlModel:UmlModel,graph:IGraph): IGraph {
        umlModel.actors.forEach(actor => {
            let actorNode: INode = graph.createNode({ tag: actor.id });
            let label = graph.addLabel({
                owner: actorNode,
                text: actor.name || "No name",
                layoutParameter: InteriorStretchLabelModel.NORTH
            });
            graph.setNodeLayout(actorNode,new Rect(0,0,200,20))
        });
        umlModel.aggregations.forEach(aggregation => {
            //console.log(aggregation.whole.id)
            //console.log(graph.nodes.get(4).tag)
            //graph.nodes.forEach(node => console.log(node.tag))
            let wholeNode: INode | null = graph.nodes.find(node => node.tag === aggregation.whole.actor.id);
            let partNode: INode | null = graph.nodes.find(node => node.tag === aggregation.part.actor.id);
            if(wholeNode !== null && partNode !== null) {
                let edge = graph.createEdge(partNode,wholeNode)
                if(aggregation.whole.name) {
                    graph.addLabel({
                        owner: edge,
                        text: aggregation.whole.name || "",//, //TODO: should be possible to do in a more clever way
                        layoutParameter: NinePositionsEdgeLabelModel.TARGET_ABOVE
                    })
                }
                if(aggregation.part.name) {
                    graph.addLabel({
                        owner: edge,
                        text: aggregation.part.name || "", //TODO: should be possible to do in a more clever way
                        layoutParameter: NinePositionsEdgeLabelModel.SOURCE_ABOVE
                    })
                }
                if(aggregation.whole.cardinality) {
                    graph.addLabel({
                        owner: edge,
                        text: aggregation.whole.cardinality || "",
                        layoutParameter: NinePositionsEdgeLabelModel.TARGET_BELOW
                    })
                }
                if(aggregation.part.cardinality) {
                    graph.addLabel({
                        owner: edge,
                        text: aggregation.part.cardinality || "", 
                        layoutParameter: NinePositionsEdgeLabelModel.SOURCE_BELOW
                    })
                }
            }
        })
        umlModel.generalizationSets.forEach(set => {
            let parentNode: INode | null = graph.nodes.find(node => node.tag === set.parent.id);
            set.children.forEach(child => {
                let childNode: INode | null = graph.nodes.find(node => node.tag === child.id);
                if(parentNode !== null && childNode !== null) {
                    let edge = graph.createEdge(childNode,parentNode);
                    if(set.name) {
                        graph.addLabel({
                            owner: edge,
                            text: set.name || "", //TODO: should be possible to do in a more clever way
                            layoutParameter: NinePositionsEdgeLabelModel.CENTER_ABOVE
                        })  
                    }
                }
            })
        })
        return graph;
    }

}